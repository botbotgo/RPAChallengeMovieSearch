UiPath - [RPA Challenge Movie Search](http://www.rpachallenge.com/movieSearch)

<a href="https://www.youtube.com/watch?v=0ycw-gGj2A0&t=933s
" target="_blank"><img src="thumbnail.png?raw=true" 
alt="YouTube Link to Complete Explanation" width="400" border="10" /></a>


Instructions

1. The goal of this challenge is to create a workflow that will check which movie review is positive or negative.


2. Add 3 movies to your list in order to start the challenge. You can either search for them or get a list of 3 popular movies


3. Click on each movie in your Movie List in order to see the reviews. Check each review and see if it is positive or negative. Once you have checked all the reviews, press on Submit in order to see your score.